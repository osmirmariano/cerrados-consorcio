let valoresImobiliario = new Array();
let valoresAutomovel = new Array();
let tipoConsorcio = new Array();
let parcelas = new Array();
let imovel, creditoCapital, parc;

valoresImobiliario.push(
  {
    credito: "R$ 70.000,00",
    parcela1_12: "R$ 659,19",
    parcela13_24: "R$ 484,17",
    demais: "R$ 419,20"
  },
  {
    credito: "R$ 80.000,00",
    parcela1_12: "R$ 753,33",
    parcela13_24: "R$ 553,33",
    demais: "R$ 479,09"
  },
  {
    credito: "R$ 90.000,00",
    parcela1_12: "R$ 847,50",
    parcela13_24: "R$ 622,50",
    demais: "R$ 538,98"
  },
  {
    credito: "R$ 100.000,00",
    parcela1_12: "R$ 941,67",
    parcela13_24: "R$ 691,67",
    demais: "R$ 598,86"
  },
  {
    credito: "R$ 110.000,00",
    parcela1_12: "R$ 1.035,83",
    parcela13_24: "R$ 760,83",
    demais: "R$ 658,75"
  },
  {
    credito: "R$ 120.000,00",
    parcela1_12: "R$ 1.130,00",
    parcela13_24: "R$ 830,00",
    demais: "R$ 718,64"
  },
  {
    credito: "R$ 130.000,00",
    parcela1_12: "R$ 1.224,17",
    parcela13_24: "R$ 899,17",
    demais: "R$ 778,52"
  },
  {
    credito: "R$ 140.000,00",
    parcela1_12: "R$ 1.318,33",
    parcela13_24: "R$ 968,33",
    demais: "R$ 838,41"
  },
  {
    credito: "R$ 150.000,00 ",
    parcela1_12: "R$ 1.412,50",
    parcela13_24: "R$ 1.037,50",
    demais: "R$ 898,30"
  },
  {
    credito: "R$ 160.000,00 ",
    parcela1_12: "R$ 1.506,67",
    parcela13_24: "R$ 1.106,67",
    demais: "R$ 958,18"
  },
  {
    credito: "R$ 170.000,00 ",
    parcela1_12: "R$ 1.600,83",
    parcela13_24: "R$ 1.175,83",
    demais: "R$ 1.018,07"
  },
  {
    credito: "R$ 180.000,00 ",
    parcela1_12: "R$ 1.695,00",
    parcela13_24: "R$ 1.245,00",
    demais: "R$ 1.077,95"
  },
  {
    credito: "R$ 190.000,00",
    parcela1_12: "R$ 1.789,17",
    parcela13_24: "R$ 1.314,17",
    demais: "R$ 1.137,84"
  },
  {
    credito: "R$ 200.000,00",
    parcela1_12: "R$ 1.883,33",
    parcela13_24: "R$ 1.383,33",
    demais: "R$ 1.197,73"
  },
  {
    credito: "R$ 210.000,00 ",
    parcela1_12: "R$ 1.977,50",
    parcela13_24: "R$ 1.452,50",
    demais: "R$ 1.257,61"
  },
  {
    credito: "R$ 220.000,00",
    parcela1_12: "R$ 2.071,67",
    parcela13_24: "R$ 1.521,67",
    demais: "R$ 1.317,50"
  },
  {
    credito: "R$ 230.000,00",
    parcela1_12: "R$ 2.165,83",
    parcela13_24: "R$ 1.590,83",
    demais: "R$ 1.377,39"
  },
  {
    credito: "R$ 240.000,00",
    parcela1_12: "R$ 2.260,00",
    parcela13_24: "R$ 1.660,00",
    demais: "R$ 1.437,27"
  },
  {
    credito: "R$ 250.000,00",
    parcela1_12: "R$ 2.354,17",
    parcela13_24: "R$ 1.729,17",
    demais: "R$ 1.497,16"
  },
  {
    credito: "R$ 260.000,00 ",
    parcela1_12: "R$ 2.448,33",
    parcela13_24: "R$ 1.798,33",
    demais: "R$ 1.557,05"
  },
  {
    credito: "R$ 270.000,00",
    parcela1_12: "R$ 2.542,50",
    parcela13_24: "R$ 1.867,50",
    demais: "R$ 1.616,93"
  },
  {
    credito: "R$ 280.000,00",
    parcela1_12: "R$ 2.636,67",
    parcela13_24: "R$ 1.936,67",
    demais: "R$ 1.676,82"
  },
  {
    credito: "R$ 290.000,00",
    parcela1_12: "R$ 2.730,83",
    parcela13_24: "R$ 2.005,83",
    demais: "R$ 1.736,70"
  },
  {
    credito: "R$ 300.000,00",
    parcela1_12: "R$ 2.825,00",
    parcela13_24: "R$ 2.075,00",
    demais: "R$ 1.796,59"
  },
  {
    credito: "R$ 400.000,00",
    parcela1_12: "R$ 3.766,67",
    parcela13_24: "R$ 2.766,67",
    demais: "R$ 2.350,00"
  },
  {
    credito: "R$ 450.000,00",
    parcela1_12: "R$ 4.237,50",
    parcela13_24: "R$ 3.112,50",
    demais: "R$ 2.643,75"
  },
  {
    credito: "R$ 500.000,00 ",
    parcela1_12: "R$ 4.708,33",
    parcela13_24: "R$ 3.458,33",
    demais: "R$ 2.937,50"
  },
  {
    credito: "R$ 550.000,00",
    parcela1_12: "R$ 5.179,17",
    parcela13_24: "R$ 3.804,17",
    demais: "R$ 3.231,25"
  },
  {
    credito: "R$ 600.000,00",
    parcela1_12: "R$ 5.650,00",
    parcela13_24: "R$ 4.150,00",
    demais: "R$ 3.525,00"
  },
  {
    credito: "R$ 650.000,00 ",
    parcela1_12: "R$ 6.120,83",
    parcela13_24: "R$ 4.495,83",
    demais: "R$ 3.818,75"
  },
  {
    credito: "R$ 700.000,00",
    parcela1_12: "R$ 6.591,67",
    parcela13_24: "R$ 4.841,67",
    demais: "R$ 4.112,50"
  }
);

valoresAutomovel.push(
  {
    credito: "R$ 25.000,00",
    parcela1_12: "R$ 426,06",
    parcela13_24: "R$ 363,54",
    demais: "R$ 366,52"
  },
  {
    credito: "R$ 30.000,00",
    parcela1_12: "R$ 511,25",
    parcela13_24: "R$ 436,25",
    demais: "R$ 439,82"
  },
  {
    credito: "R$ 35.000,00",
    parcela1_12: "R$ 596,46",
    parcela13_24: "R$ 508,96",
    demais: "R$ 513,13"
  },
  {
    credito: "R$ 40.000,00",
    parcela1_12: "R$ 681,67",
    parcela13_24: "R$ 581,67",
    demais: "R$ 586,43"
  },
  {
    credito: "R$ 45.000,00",
    parcela1_12: "R$ 766,88",
    parcela13_24: "R$ 654,38",
    demais: "R$ 659,73"
  },
  {
    credito: "R$ 50.000,00",
    parcela1_12: "R$ 852,08",
    parcela13_24: "R$ 727,08",
    demais: "R$ 733,04"
  },
  {
    credito: "R$ 55.000,00",
    parcela1_12: "R$ 937,29",
    parcela13_24: "R$ 799,79",
    demais: "R$ 806,34"
  },
  {
    credito: "R$ 60.000,00",
    parcela1_12: "R$ 1.022,50",
    parcela13_24: "R$ 872,50",
    demais: "R$ 879,64"
  },
  {
    credito: "R$ 65.000,00",
    parcela1_12: "R$ 1.107,71",
    parcela13_24: "R$ 945,21",
    demais: "R$ 952,95"
  },
  {
    credito: "R$ 70.000,00",
    parcela1_12: "R$ 1.192,92",
    parcela13_24: "R$ 1.017,92",
    demais: "R$ 1.026,25"
  },
  {
    credito: "R$ 75.000,00",
    parcela1_12: "R$ 1.278,13",
    parcela13_24: "R$ 1.090,63",
    demais: "R$ 1.099,55"
  },
  {
    credito: "R$ 80.000,00",
    parcela1_12: "R$ 1.363,33",
    parcela13_24: "R$ 1.163,33",
    demais: "R$ 1.172,86"
  },
  {
    credito: "R$ 85.000,00 ",
    parcela1_12: "R$ 1.448,54",
    parcela13_24: "R$ 1.236,04",
    demais: "R$ 1.246,16"
  },
  {
    credito: "R$ 90.000,00",
    parcela1_12: "R$ 1.533,75",
    parcela13_24: "R$ 1.308,75",
    demais: "R$ 1.319,46"
  },
  {
    credito: "R$ 95.000,00",
    parcela1_12: "R$ 1.618,96",
    parcela13_24: "R$ 1.381,46",
    demais: "R$ 1.392,77"
  },
  {
    credito: "R$ 100.000,00",
    parcela1_12: "R$ 1.704,17",
    parcela13_24: "R$ 1.454,17",
    demais: "R$ 1.466,07"
  },
  {
    credito: "R$ 110.000,00",
    parcela1_12: "R$ 1.874,58",
    parcela13_24: "R$ 1.599,58",
    demais: "R$ 1.612,68"
  },
  {
    credito: "R$ 115.000,00",
    parcela1_12: "R$ 1.959,79",
    parcela13_24: "R$ 1.672,29",
    demais: "R$ 1.685,98"
  },
  {
    credito: "R$ 120.000,00",
    parcela1_12: "R$ 2.045,00",
    parcela13_24: "R$ 1.745,00",
    demais: "R$ 1.759,29"
  },
  {
    credito: "R$ 125.000,00 ",
    parcela1_12: "R$ 2.130,21",
    parcela13_24: "R$ 1.817,71",
    demais: "R$ 1.832,59"
  },
  {
    credito: "R$ 130.000,00",
    parcela1_12: "R$ 2.215,42",
    parcela13_24: "R$ 1.890,42",
    demais: "R$ 1.905,89"
  },
  {
    credito: "R$ 135.000,00",
    parcela1_12: "R$ 2.300,63",
    parcela13_24: "R$ 1.963,13",
    demais: "R$ 1.979,20"
  },
  {
    credito: "R$ 140.000,00",
    parcela1_12: "R$ 2.385,83",
    parcela13_24: "R$ 2.035,83",
    demais: "R$ 2.052,50"
  },
  {
    credito: "R$ 145.000,00",
    parcela1_12: "R$ 2.471,04",
    parcela13_24: "R$ 2.108,54",
    demais: "R$ 2.125,80"
  },
  {
    credito: "R$ 150.000,00",
    parcela1_12: "R$ 2.556,25",
    parcela13_24: "R$ 2.181,25",
    demais: "R$ 2.199,11"
  }
);

tipoConsorcio.push(
  {
    nome: "Imóvel",
    value: "1"
  },
  {
    nome: "Automóvel",
    value: "2"
  }
);

parcelas.push(
  {
    parcela: "1 a 12",
    value: "1"
  },
  {
    parcela: "13 a 24",
    value: "2"
  }
);

var habilitar = 0;

tipoConsorcio.forEach(function(item) {
  addOptionTipo(item.nome);
});

function tipo() {
  var e = document.getElementById("tipoConsorcio");
  var itemSelecionado = e.options[e.selectedIndex].value;
  imovel = itemSelecionado;
  if (itemSelecionado == "Automóvel") {
    $("#valorCredito").empty();
    valoresAutomovel.forEach(function(item) {
      addOption(item.credito);
    });

    // parcelas.forEach(function(item) {
    //   addOptionParcelas(item.parcela);
    // });
  } 
  else {
    if (itemSelecionado == "Imóvel") {
      $("#valorCredito").empty();
      valoresImobiliario.forEach(function(item) {
        addOption(item.credito);
      });

      parcelas.forEach(function(item) {
        addOptionParcelas(item.parcela);
      });
    }
  }
}

/**
 * Função 
 */
// function parcelamento() {
//   var e = document.getElementById("valorParcelas");
//   var itemSelecionadoParcela = e.options[e.selectedIndex].value;
//   creditoCapital = itemSelecionadoParcela;
//   var display = document.getElementById("btnC").style.display;
//   if (display == "none") {
//     document.getElementById("btnC").style.display = "block";
//   }
// }

function valores() {
  var e = document.getElementById("valorCredito");
  var itemSelecionadoValor = e.options[e.selectedIndex].value;
  parc = itemSelecionadoValor;
  var display = document.getElementById("btnC").style.display;
  if (display == "none") {
    document.getElementById("btnC").style.display = "block";
  }
}

function calcular() {
  console.log(creditoCapital);
  if (imovel == "Automóvel") {
    valoresAutomovel.forEach(function(item) {
      console.log('ITEM> ', item);
      if (item.credito == parc) {
        document.getElementById("consorcio").innerHTML = imovel;
        document.getElementById("credito").innerHTML = item.credito;
        document.getElementById("valorPar").innerHTML = item.parcela1_12;
        document.getElementById("demais").innerHTML = item.demais;
        document.getElementById("parcelas").innerHTML = '1 a 12';

        document.getElementById("consorcio2").innerHTML = imovel;
        document.getElementById("credito2").innerHTML = item.credito;
        document.getElementById("valorPar2").innerHTML = item.parcela13_24
        document.getElementById("demais2").innerHTML = item.demais;
        document.getElementById("parcelas2").innerHTML = '13 a 24';
      }
    });
  } 
  else {
    if (imovel == "Imóvel") {
      valoresImobiliario.forEach(function(item) {
        if (item.credito == parc) {
            document.getElementById("consorcio").innerHTML = imovel;
            document.getElementById("credito").innerHTML = item.credito;
            document.getElementById("valorPar").innerHTML = item.parcela1_12;
            document.getElementById("demais").innerHTML = item.demais;
            document.getElementById("parcelas").innerHTML = '1 a 12';

            document.getElementById("consorcio2").innerHTML = imovel;
            document.getElementById("credito2").innerHTML = item.credito;
            document.getElementById("valorPar2").innerHTML = item.parcela13_24
            document.getElementById("demais2").innerHTML = item.demais;
            document.getElementById("parcelas2").innerHTML = '13 a 24';
        }
      });
    }
  }

  var display = document.getElementById("mostra").style.display;
  if (display == "none") {
    document.getElementById("mostra").style.display = "block";
  }
}

function addOptionTipo(valor) {
  var option = new Option(valor, valor);
  var select = document.getElementById("tipoConsorcio");
  select.add(option, 'value="' + valor.value + '"');
}

function addOption(valor) {
  var option = new Option(valor, valor);
  var select = document.getElementById("valorCredito");
  select.add(option, 'value="' + valor.parcela1_12 + '"');
}

function addOptionParcelas(valor) {
  var option = new Option(valor, valor);
  var select = document.getElementById("valorParcelas");
  select.add(option, 'value="' + valor.value + '"');
}
